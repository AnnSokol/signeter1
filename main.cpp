#include <iostream>

using namespace std;

void a()
{ cout <<  "    //\\ " << endl;
  cout <<  "   //  \\ "<< endl;
  cout <<  "  //----\\"<< endl;
  cout <<  " //      \\" << endl;

}

void n()
{ cout <<"||--||" << endl;
  cout <<"||  ||" << endl;
  cout <<"||  ||_"<< endl;
}

void y()
{ cout << " \\\\      //" << endl;
  cout << "  \\\\    // " << endl;
  cout << "   \\\\  //" << endl;
  cout << "    \\\\//" << endl;
  cout << "     //" << endl;
  cout << "    //" << endl;
}

int main()

{
    a();
    n();
    y();
    return 0;
}
